package com.parkinglot.exception;

public class ParkingException extends RuntimeException{
    public ParkingException() {
        super("No available position");
    }
}
