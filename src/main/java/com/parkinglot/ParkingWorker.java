package com.parkinglot;

import com.parkinglot.exception.UnrecognizedTicketException;

import java.util.ArrayList;
import java.util.List;

public abstract class ParkingWorker {
    protected List<ParkingLot> parkingLots;

    public ParkingWorker() {
        this.parkingLots = new ArrayList<>();
    }

    public abstract Ticket park(Car car);

    public Car fetch(Ticket ticket) {
        Car car = parkingLots.stream()
                .filter(parkingLot -> parkingLot.inPosition(ticket))
                .findFirst()
                .map(parkingLot -> parkingLot.fetch(ticket))
                .orElseThrow(UnrecognizedTicketException::new);

        return car;
    }

    protected void addParkingLot(ParkingLot parkingLot) {
        parkingLots.add(parkingLot);
    }

}
