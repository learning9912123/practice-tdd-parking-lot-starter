package com.parkinglot.strategy.manager;

import com.parkinglot.Car;
import com.parkinglot.ParkingLot;
import com.parkinglot.ParkingWorker;
import com.parkinglot.Ticket;
import com.parkinglot.exception.ParkingException;
import com.parkinglot.strategy.boy.ParkingStrategy;

import java.util.List;

public class LazyManagerParkingStrategy implements ManagerParkingStrategy {

    public LazyManagerParkingStrategy() {
    }

    @Override
    public Ticket park(List<ParkingWorker> parkingWorkers, ParkingStrategy selfParkingStrategy
            , List<ParkingLot> selfParkingLot, Car car) {
        for (ParkingWorker parkingWorker : parkingWorkers) {
            try {
                return parkingWorker.park(car);
            } catch (ParkingException ignore) {
            }
        }
        return selfParkingStrategy.park(selfParkingLot,car);
    }
}
