package com.parkinglot.strategy.manager;

import com.parkinglot.Car;
import com.parkinglot.ParkingLot;
import com.parkinglot.ParkingWorker;
import com.parkinglot.Ticket;
import com.parkinglot.strategy.boy.ParkingStrategy;

import java.util.List;

public interface ManagerParkingStrategy {
    Ticket park(List<ParkingWorker> parkingWorkers, ParkingStrategy selfParkingStrategy
            ,List<ParkingLot> selfParkingLot, Car car);
}
