package com.parkinglot.strategy.boy;

import com.parkinglot.Car;
import com.parkinglot.ParkingLot;
import com.parkinglot.Ticket;
import com.parkinglot.exception.ParkingException;

import java.util.Comparator;
import java.util.List;

public class SmartParkingStrategy implements ParkingStrategy {

    @Override
    public Ticket park(List<ParkingLot> parkingLots, Car car) {
        Ticket ticket = parkingLots.stream()
                .max(Comparator.comparingInt(ParkingLot::remainPosition))
                .map(parkingLot -> parkingLot.park(car))
                .orElseThrow(ParkingException::new);
        return ticket;
    }
}
