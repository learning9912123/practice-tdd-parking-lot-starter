package com.parkinglot.strategy.boy;

import com.parkinglot.Car;
import com.parkinglot.ParkingLot;
import com.parkinglot.Ticket;
import com.parkinglot.exception.ParkingException;

import java.util.List;

public class FoolParkingStrategy implements ParkingStrategy{

    @Override
    public Ticket park(List<ParkingLot> parkingLots, Car car) {
        Ticket ticket = parkingLots.stream()
                .filter(ParkingLot::hasPosition)
                .findFirst()
                .map(parkingLot -> parkingLot.park(car))
                .orElseThrow(ParkingException::new);
        return ticket;
    }
}
