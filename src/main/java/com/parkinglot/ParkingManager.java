package com.parkinglot;

import com.parkinglot.exception.UnrecognizedTicketException;
import com.parkinglot.strategy.boy.FoolParkingStrategy;
import com.parkinglot.strategy.boy.ParkingStrategy;
import com.parkinglot.strategy.manager.LazyManagerParkingStrategy;
import com.parkinglot.strategy.manager.ManagerParkingStrategy;

import java.util.ArrayList;
import java.util.List;

public class ParkingManager extends ParkingWorker {
    private List<ParkingWorker> parkingWorkers;
    private ManagerParkingStrategy managerParkingStrategy;

    private ParkingStrategy parkingStrategy;

    public ParkingManager() {
        super();
        parkingWorkers = new ArrayList<>();
        this.managerParkingStrategy = new LazyManagerParkingStrategy();
        parkingStrategy = new FoolParkingStrategy();
    }

    public ParkingManager(ManagerParkingStrategy strategy,ParkingStrategy parkingStrategy) {
        super();
        parkingWorkers = new ArrayList<>();
        this.managerParkingStrategy = strategy;
        this.parkingStrategy = parkingStrategy;
    }

    @Override
    public Ticket park(Car car) {
        return managerParkingStrategy.park(parkingWorkers, parkingStrategy,parkingLots,car);
    }

    @Override
    public Car fetch(Ticket ticket) {
        for (ParkingWorker parkingWorker : parkingWorkers) {
            try {
                return parkingWorker.fetch(ticket);
            }catch (UnrecognizedTicketException ignore){}
        }
        return super.fetch(ticket);
    }

    public void addParkingWorker(ParkingWorker worker) {
        parkingWorkers.add(worker);
    }

}
