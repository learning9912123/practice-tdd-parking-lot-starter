package com.parkinglot;


import com.parkinglot.strategy.boy.ParkingStrategy;

public class ParkingBoy extends ParkingWorker{

    private ParkingStrategy parkingStrategy;
    public ParkingBoy(ParkingStrategy strategy) {
        super();
        this.parkingStrategy = strategy;
    }
    public ParkingBoy() {
        super();
    }
    @Override
    public Ticket park(Car car){
        return parkingStrategy.park(parkingLots,car);
    }





}
