package com.parkinglot;

import com.parkinglot.exception.ParkingException;
import com.parkinglot.exception.UnrecognizedTicketException;

import java.util.HashMap;
import java.util.Map;

public class ParkingLot {
    private Map<Ticket, Car> positions;
    private final int maxCount;

    public ParkingLot() {
        positions = new HashMap<>();
        maxCount = 10;
    }

    public ParkingLot(int count) {
        this.maxCount = count;
        positions = new HashMap<>();
    }

    public boolean hasPosition() {
        return positions.size() < maxCount;
    }

    public boolean inPosition(Ticket ticket) {
        return positions.containsKey(ticket);
    }

    public int remainPosition() {
        return maxCount - positions.size();
    }

    public float remainRate() {
        return (float) remainPosition() / maxCount;
    }


    public Ticket park(Car car) {
        if (!hasPosition()) {
            throw new ParkingException();
        }
        Ticket ticket = new Ticket();
        positions.put(ticket, car);
        return ticket;
    }

    public Car fetch(Ticket ticket) {
        if (!positions.containsKey(ticket)) {
            throw new UnrecognizedTicketException();
        }
        return positions.remove(ticket);
    }


}
