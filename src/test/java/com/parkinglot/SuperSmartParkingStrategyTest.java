package com.parkinglot;

import com.parkinglot.strategy.boy.SuperSmartParkingStrategy;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class SuperSmartParkingStrategyTest {
    @Test
    public void test_superSmartParkingBoy_strategy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(4);
        parkingLot1.park(new Car());
        parkingLot1.park(new Car());

        ParkingLot parkingLot2 = new ParkingLot(5);
        parkingLot2.park(new Car());
        parkingLot2.park(new Car());

        ParkingBoy boy = new ParkingBoy(new SuperSmartParkingStrategy());
        boy.addParkingLot(parkingLot1);
        boy.addParkingLot(parkingLot2);

        //when
        Ticket ticket = boy.park(new Car());

        //then
        assertTrue(parkingLot2.inPosition(ticket));
    }
}
