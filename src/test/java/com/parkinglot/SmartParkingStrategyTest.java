package com.parkinglot;

import com.parkinglot.strategy.boy.SmartParkingStrategy;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class SmartParkingStrategyTest {
    @Test
    public void test_smartParkingBoy_strategy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(3);
        parkingLot1.park(new Car());

        ParkingLot parkingLot2 = new ParkingLot(2);
        parkingLot2.park(new Car());

        ParkingBoy boy = new ParkingBoy(new SmartParkingStrategy());
        boy.addParkingLot(parkingLot1);
        boy.addParkingLot(parkingLot2);

        //when
        Ticket ticket = boy.park(new Car());

        //then
        assertTrue(parkingLot1.inPosition(ticket));
    }

}
