package com.parkinglot;

import com.parkinglot.exception.ParkingException;
import com.parkinglot.exception.UnrecognizedTicketException;
import com.parkinglot.strategy.boy.FoolParkingStrategy;
import com.parkinglot.strategy.boy.SmartParkingStrategy;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;


import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class StandardParkingBoyTest {

    public static Stream<Arguments> testBoyData() {
        return Stream.of(
                Arguments.of(new ParkingBoy(new FoolParkingStrategy())),
                Arguments.of(new ParkingBoy(new SmartParkingStrategy())),
                Arguments.of(new ParkingBoy(new SmartParkingStrategy())),
                Arguments.of(new ParkingManager())
        );
    }

    @ParameterizedTest()
    @MethodSource("testBoyData")
    public void should_return_a_ticket_when_park_car_given_car_and_boy(ParkingWorker boy) {
        Car car = new Car();
        boy.addParkingLot(new ParkingLot());
        Ticket ticket = boy.park(car);

        assertNotNull(ticket);
    }


    @ParameterizedTest()
    @MethodSource("testBoyData")
    public void should_return_a_car_when_fetch_given_ticket_and_boy(ParkingWorker boy) {
        //given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();
        boy.addParkingLot(parkingLot);
        Ticket ticket = boy.park(car);

        //when
        Car fetchCar = boy.fetch(ticket);

        //then
        assertEquals(car, fetchCar);
    }

    @ParameterizedTest()
    @MethodSource("testBoyData")
    public void should_return_right_car_when_fetch_given_one_of_tickets_and_boy(ParkingWorker boy) {
        //given
        Car car1 = new Car();
        Car car2 = new Car();
        boy.addParkingLot(new ParkingLot());
        Ticket ticket1 = boy.park(car1);
        Ticket ticket2 = boy.park(car2);

        //when
        Car fetchCar1 = boy.fetch(ticket1);
        Car fetchCar2 = boy.fetch(ticket2);

        //then
        assertEquals(car1, fetchCar1);
        assertEquals(car2, fetchCar2);
    }


    @ParameterizedTest()
    @MethodSource("testBoyData")
    public void should_throw_unrecognizedTicketException_when_fetch_given_wrong_tickets_and_boy(ParkingWorker boy) {
        //given
        Car car = new Car();
        boy.addParkingLot(new ParkingLot());
        boy.park(car);


        //when
        //then
        UnrecognizedTicketException unrecognizedTicketException = assertThrows(UnrecognizedTicketException.class, () -> boy.fetch(new Ticket()));
        assertEquals("Unrecognized parking ticket", unrecognizedTicketException.getMessage());
    }


    @ParameterizedTest()
    @MethodSource("testBoyData")
    public void should_throw_unrecognizedTicketException_when_fetch_given_used_tickets_and_boy(ParkingWorker boy) {
        //given
        Car car = new Car();
        boy.addParkingLot(new ParkingLot());
        boy.park(car);

        //when
        //then
        UnrecognizedTicketException unrecognizedTicketException = assertThrows(UnrecognizedTicketException.class, () -> boy.fetch(new Ticket()));
        assertEquals("Unrecognized parking ticket", unrecognizedTicketException.getMessage());
    }

    @ParameterizedTest()
    @MethodSource("testBoyData")
    public void should_throw_parkingException_when_park_given__boy_no_position(ParkingWorker boy) {
        //given
        Car car1 = new Car();
        Car car2 = new Car();
        boy.addParkingLot(new ParkingLot(2));

        //when
        boy.park(car1);
        boy.park(car2);

        ParkingException parkingException = assertThrows(ParkingException.class, () -> boy.park(new Car()));
        assertEquals("No available position", parkingException.getMessage());
    }


    @ParameterizedTest()
    @MethodSource("testBoyData")
    public void should_return_ticket_when_park_given_a_car_and_boy_has_parkingLots_no_fulled(ParkingWorker boy) {
        //given
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(2);

        boy.addParkingLot(parkingLot1);
        boy.addParkingLot(parkingLot2);

        //when then
        boy.park(new Car());
        boy.park(new Car());

        Ticket ticket = boy.park(new Car());
        assertNotNull(ticket);
    }

    @ParameterizedTest()
    @MethodSource("testBoyData")
    public void should_return_right_car_when_fetch_given_one_of_tickets_and_boy_has_parkingLots(ParkingWorker boy) {
        //given
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(2);

        boy.addParkingLot(parkingLot1);
        boy.addParkingLot(parkingLot2);

        Ticket ticket1 = boy.park(car1);
        Ticket ticket2 = boy.park(car2);

        //when
        Car fetchCar1 = boy.fetch(ticket1);
        Car fetchCar2 = boy.fetch(ticket2);

        //then
        assertEquals(car1, fetchCar1);
        assertEquals(car2, fetchCar2);
    }

    @ParameterizedTest()
    @MethodSource("testBoyData")
    public void should_throw_UnrecognizedTicketException_when_fetch_given_wrong_tickets_and_boy_has_parkingLots(ParkingWorker boy) {
        //given
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(2);

        boy.addParkingLot(parkingLot1);
        boy.addParkingLot(parkingLot2);

        boy.park(car1);
        boy.park(car2);

        //when
        //then
        UnrecognizedTicketException unrecognizedTicketException = assertThrows(UnrecognizedTicketException.class, () -> boy.fetch(new Ticket()));
        assertEquals("Unrecognized parking ticket", unrecognizedTicketException.getMessage());
    }

    @ParameterizedTest()
    @MethodSource("testBoyData")
    public void should_throw_UnrecognizedTicketException_when_fetch_given_used_tickets_and_parkingBoy_has_parkingLots(ParkingWorker boy) {
        //given
        Car car1 = new Car();
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(2);
        boy.addParkingLot(parkingLot1);
        boy.addParkingLot(parkingLot2);
        Ticket ticket = boy.park(car1);
        boy.fetch(ticket);

        //when
        //then
        UnrecognizedTicketException unrecognizedTicketException = assertThrows(UnrecognizedTicketException.class, () -> boy.fetch(ticket));
        assertEquals("Unrecognized parking ticket", unrecognizedTicketException.getMessage());
    }

    @ParameterizedTest()
    @MethodSource("testBoyData")
    public void should_throw_parkingException_when_park_given_a_car_boy_has_parkingLots_fulled(ParkingWorker boy) {
        //given
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(2);

        boy.addParkingLot(parkingLot1);
        boy.addParkingLot(parkingLot2);

        //when then
        boy.park(new Car());
        boy.park(new Car());
        boy.park(new Car());
        boy.park(new Car());
        ParkingException parkingException = assertThrows(ParkingException.class, () -> boy.park(new Car()));
        assertEquals("No available position", parkingException.getMessage());
    }

}
