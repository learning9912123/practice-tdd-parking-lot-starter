package com.parkinglot;

import com.parkinglot.exception.ParkingException;
import com.parkinglot.exception.UnrecognizedTicketException;
import com.parkinglot.strategy.boy.FoolParkingStrategy;
import com.parkinglot.strategy.boy.SmartParkingStrategy;
import com.parkinglot.strategy.boy.SuperSmartParkingStrategy;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingMangerTest {


    @Test
    public void TestManagerAssignPark() {
        //given
        ParkingBoy foolBoy = new ParkingBoy(new FoolParkingStrategy());
        ParkingBoy smartBoy = new ParkingBoy(new SmartParkingStrategy());
        ParkingBoy superSmartBoy = new ParkingBoy(new SuperSmartParkingStrategy());

        foolBoy.addParkingLot(new ParkingLot(1));
        smartBoy.addParkingLot(new ParkingLot(1));
        superSmartBoy.addParkingLot(new ParkingLot(1));

        ParkingManager parkingManager = new ParkingManager();
        parkingManager.addParkingWorker(foolBoy);
        parkingManager.addParkingWorker(smartBoy);
        parkingManager.addParkingWorker(superSmartBoy);
        parkingManager.addParkingLot(new ParkingLot(1));
        //when
        Ticket ticket1 = parkingManager.park(new Car());
        Ticket ticket2 = parkingManager.park(new Car());
        Ticket ticket3 = parkingManager.park(new Car());
        Ticket ticket4 = parkingManager.park(new Car());

        //then
        assertNotNull(ticket1);
        assertNotNull(ticket2);
        assertNotNull(ticket3);
        assertNotNull(ticket4);

        ParkingException parkingException = assertThrows(ParkingException.class, () -> parkingManager.park(new Car()));
        assertEquals("No available position", parkingException.getMessage());
    }

    @Test
    public void TestManagerFetch(){
        //given
        ParkingBoy foolBoy = new ParkingBoy(new FoolParkingStrategy());
        ParkingBoy smartBoy = new ParkingBoy(new SmartParkingStrategy());
        ParkingBoy superSmartBoy = new ParkingBoy(new SuperSmartParkingStrategy());

        foolBoy.addParkingLot(new ParkingLot(1));
        smartBoy.addParkingLot(new ParkingLot(1));
        superSmartBoy.addParkingLot(new ParkingLot(1));

        ParkingManager parkingManager = new ParkingManager();
        parkingManager.addParkingWorker(foolBoy);
        parkingManager.addParkingWorker(smartBoy);
        parkingManager.addParkingWorker(superSmartBoy);
        parkingManager.addParkingLot(new ParkingLot(1));

        //when
        Car car1 = new Car();
        Car car2= new Car();
        Car car3 = new Car();
        Car car4 = new Car();

        Ticket ticket1 = parkingManager.park(car1);
        Ticket ticket2 = parkingManager.park(car2);
        Ticket ticket3 = parkingManager.park(car3);
        Ticket ticket4 = parkingManager.park(car4);

        //then
        assertEquals(car1,parkingManager.fetch(ticket1));
        assertEquals(car2,parkingManager.fetch(ticket2));
        assertEquals(car3,parkingManager.fetch(ticket3));
        assertEquals(car4,parkingManager.fetch(ticket4));


        //used ticket
        UnrecognizedTicketException unrecognizedTicketException1 = assertThrows(UnrecognizedTicketException.class, () -> parkingManager.fetch(ticket1));
        assertEquals("Unrecognized parking ticket", unrecognizedTicketException1.getMessage());

        //error ticket
        UnrecognizedTicketException unrecognizedTicketException2 = assertThrows(UnrecognizedTicketException.class, () -> parkingManager.fetch(new Ticket()));
        assertEquals("Unrecognized parking ticket", unrecognizedTicketException2.getMessage());
    }
}
